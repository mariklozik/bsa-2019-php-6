<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        // TODO: Implement
        $baseUrl = 'http://icons.iconarchive.com/icons/';

        $parsedProductList = [
            [
                'id' => 1,
                'name' => 'Cheese',
                'price' => 2.5,
                'imageUrl' => $baseUrl . 'aha-soft/desktop-buffet/256/Cheese-icon.png',
                'rating' => 1,

            ],
            [
                'id' => 2,
                'name' => 'Pizza',
                'price' => 5.5,
                'imageUrl' => $baseUrl . 'sonya/swarm/256/Pizza-icon.png',
                'rating' => 2,

            ],
            [
                'id' => 3,
                'name' => 'Cake',
                'price' => 3.5,
                'imageUrl' => $baseUrl . 'chrisl21/minecraft/256/Cake-icon.png',
                'rating' => 3,

            ],
            [
                'id' => 4,
                'name' => 'Hot Dog',
                'price' => 1.5,
                'imageUrl' => $baseUrl .'sonya/swarm/256/Hot-Dog-icon.png',
                'rating' => 4,

            ],
        ];

        $productList = [];
        foreach ($parsedProductList as $product) {
            $productList[] = new Product(
                $product['id'],
                $product['name'],
                $product['price'],
                $product['imageUrl'],
                $product['rating']
            );
        }

        return $productList;
    }
}