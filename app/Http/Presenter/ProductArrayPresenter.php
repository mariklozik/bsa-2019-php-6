<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;

class ProductArrayPresenter
{
    public static function present(Product $product): array
    {
        return [
            'id'     => $product->getId(),
            'name'   => $product->getName(),
            'price'  => $product->getPrice(),
            'img'    => $product->getImageUrl(),
            'rating' => $product->getRating()
        ];
    }
}
