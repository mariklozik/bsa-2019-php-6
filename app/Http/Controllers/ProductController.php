<?php

namespace App\Http\Controllers;

use App\Action\Product\GetCheapestProductsAction;

class ProductController extends Controller
{
    // TODO: Implement methods

    public function getCheapestProducts(GetCheapestProductsAction $cheapestProductsAction)
    {
        $cheapestProducts = $cheapestProductsAction->execute()->getProducts();

        return view('cheap_products', compact('cheapestProducts'));
    }
}