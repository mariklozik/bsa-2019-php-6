<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;
use App\Repository\ProductRepositoryInterface;

class ApiProductController extends Controller
{
    public function getAllProducts(GetAllProductsAction $allProductsAction)
    {
        $products = $allProductsAction->execute()->getProducts();

        $allProducts = [];
        collect($products)->each(function ($e) use (&$allProducts) {
            $allProducts[] = ProductArrayPresenter::present($e);
        });

        return response()->json($allProducts, 200);
    }

    public function getMostPopularProducts(GetMostPopularProductAction $mostPopularProductAction)
    {
//        dump((new GetMostPopularProductAction($repository))->execute()->getProduct());
        $mostPopularProduct = $mostPopularProductAction->execute()->getProduct();

        return response()->json(ProductArrayPresenter::present($mostPopularProduct), 200);
    }
}