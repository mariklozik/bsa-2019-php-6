<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetCheapestProductsResponse
{
    // TODO: Implement methods
    private $products;

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function getProducts(): array
    {
        return $this->products;
    }
}