<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetAllProductsAction
{
    // TODO: Implement methods

    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetAllProductsResponse
    {
        // TODO: Implement

        $products = $this->repository->findAll();

        return new GetAllProductsResponse($products);
    }
}