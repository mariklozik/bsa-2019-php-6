<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    // TODO: Implement methods

    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetCheapestProductsResponse
    {
        // TODO: Implement

        $allProducts = $this->repository->findAll();
        $cheapestProducts = collect($allProducts)->sortBy(function ($e) {
            return $e->getPrice();
        })->values()->take(3)->all();

        return new GetCheapestProductsResponse($cheapestProducts);
    }
}