<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    // TODO: Implement methods

    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        // TODO: Implement

        $allProducts = $this->repository->findAll();
        $popularProduct = collect($allProducts)->sortByDesc(function ($e) {
            return $e->getRating();
        })->first();

        return new GetMostPopularProductResponse($popularProduct);
    }
}