<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>

    <style type="text/css">
        ul {
            display: flex;
            flex-direction: row;
            justify-content: space-around;
            list-style-type: none;
        }

        img {
            width: 64px;
        }
        
        h2 {
            text-align: center;
        }
    </style>
</head>
<body>

    <h2> List of cheapest products </h2>

    @if(count($cheapestProducts) > 0)
        <ul>
            @foreach ($cheapestProducts as $product)
                <li>
                    <p> {{ $product->getId() }}. {{ $product->getName() }} </p>
                    <p> <img src="{{ $product->getImageUrl() }}"> </p>
                    <p> Price: {{ $product->getPrice() }} </p>
                    <p> Rating: {{ $product->getRating() }} </p>
                </li>
            @endforeach
        </ul>
    @else
        No products
    @endif

</body>
</html>